# Build image locally

~~~
cd ./provisioning
docker build --tag bld_api -f docker/Dockerfile_debug .
~~~

# Run container with api locally

~~~
docker run -it -d --name bld_api -p 127.0.0.1:9000:9000 bld_api
~~~