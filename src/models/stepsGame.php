<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "steps_game".
 *
 * @property int $id
 * @property int $game_id
 * @property int $word_id
 * @property int $player_id 0 - игрок 1, 1 - игрок 2
 *
 * @property Games $game
 * @property Words $word
 */
class stepsGame extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'steps_game';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'word_id', 'player_id'], 'required'],
            [['game_id', 'word_id', 'player_id'], 'integer'],
            [['game_id', 'word_id'], 'unique', 'targetAttribute' => ['game_id', 'word_id']],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['word_id'], 'exist', 'skipOnError' => true, 'targetClass' => Words::className(), 'targetAttribute' => ['word_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'word_id' => 'Word ID',
            'player_id' => 'Player ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Words::className(), ['id' => 'word_id']);
    }
}
