<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "words".
 *
 * @property int $id
 * @property string $word
 * @property int $length Длина слова
 *
 * @property StepsGame[] $stepsGames
 * @property Games[] $games
 */
class Words extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'words';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['word', 'length'], 'required'],
            [['length'], 'integer'],
            [['word'], 'string', 'max' => 45],
            [['word'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word' => 'Word',
            'length' => 'Length',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStepsGames()
    {
        return $this->hasMany(StepsGame::className(), ['word_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Games::className(), ['id' => 'game_id'])->viaTable('steps_game', ['word_id' => 'id']);
    }
}
