<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "games".
 *
 * @property int $id
 * @property int $status const game_created = 0; // игра создана const one_player_in_game = 1; // игрок 1 зашел в игру const two_player_in_game = 2; // игрок 2 зашел в игру const one_player_do_step = 3; // игрок 1 делает ход const two_player_do_step = 4; // игрок 2 делает ход const one_player_win = 5; // игрок 1 выиграл const two_player_win = 6; // игрок 2 выиграл
 * @property string $auth_pl1 ключ авторизации игрока 1
 * @property string $auth_pl2 ключ авторизации игрока 2
 *
 * @property StepsPl1[] $stepsPl1s
 * @property StepsPl2[] $stepsPl2s
 */
class Games extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['auth_pl1', 'auth_pl2'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'auth_pl1' => 'Auth Pl1',
            'auth_pl2' => 'Auth Pl2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStepsPl1s()
    {
        return $this->hasMany(StepsPl1::className(), ['game_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStepsPl2s()
    {
        return $this->hasMany(StepsPl2::className(), ['game_id' => 'id']);
    }
}
