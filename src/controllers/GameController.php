<?php

namespace app\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use app\models\Games;
use app\models\stepsGame;
use app\models\Words;
use yii\filters\auth\HttpBasicAuth;
use yii\db\Command;

class GameController extends Controller
{
	
	const game_created = 1; // игра создана
	const one_player_in_game = 2; // игрок 1 зашел в игру
	const two_player_in_game = 3; // игрок 2 зашел в игру. Игрок 1 делает ход
	const two_player_do_step = 4; // игрок 2 делает ход
	const one_player_win = 5; // игрок 1 выиграл
	const two_player_win = 6; // игрок 2 выиграл
	
	public $enableCsrfValidation = false;
	
	public static function allowedDomains()
	{
		return [
			// '*',                        // star allows all domains
			'http://localhost:4200'
		];
	}	

	public function behaviors()
	{
		$behaviors = parent::behaviors();

		// remove authentication filter
		$auth = $behaviors['authenticator'];
		unset($behaviors['authenticator']);

		// add CORS filter
		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
		];

		// re-add authentication filter
		$behaviors['authenticator'] = $auth;
		// avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
		$behaviors['authenticator']['except'] = ['options'];
		$behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

		return $behaviors;
	}
	
	/*public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;		
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // restrict access to domains:
                'Origin'                           => static::allowedDomains(),
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
            ],
        ];*/
		/* remove for debug begin */
		/*$behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // restrict access to domains:
                'Origin'                           => static::allowedDomains(),                
                'Access-Control-Request-Method'    => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
            ],
        ];
        $behaviors['authenticator'] = [
                'class' => HttpBearerAuth::className(),
                'except' => ['options'],
            ];*/
        /* remove for debug end */
	/*	return $behaviors;
	}*/

    /**
     * {@inheritdoc}
     */
    /*public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }*/
	
	public function actionList()
    {
		$gamelist = Games::find()->select('id, status_id')
		->where(['<','status_id', self::two_player_in_game ])->asArray()->all();		
        return $gamelist;
    }
    
    public function actionConnect(){
		$request = Yii::$app->request;
		$game_id = (int)$request->getBodyParam('id');
		$success = false;
		$auth_key = "";
		$game = ['success' => &$success, 'auth_key' => &$auth_key];
		try {			
			
            $sql =  'CALL connect (:game_id, @success, @auth_key);' ;

            $command = Yii::$app->db->createCommand($sql);

            $command->bindParam(":game_id", $game_id, \PDO::PARAM_INT);            
            $command->execute();
            $success = (bool)Yii::$app->db->createCommand("select @success;")->queryScalar();
            $auth_key = Yii::$app->db->createCommand("select @auth_key;")->queryScalar();            

         }catch (Exception $e) {

            Log::trace("Error : ".$e);

            throw new Exception("Error : ".$e);

         }         
		return $game;
	}
	
	public function actionCheckstep(){
		$request = Yii::$app->request;
		$game_id = (int)$request->getBodyParam('id');
		$auth_key = $request->getBodyParam('auth_key');
		$word = $request->getBodyParam('word');
		$success = false;		
		$game = ['success' => &$success];
		try {			
			
            $sql =  'CALL check_step (:game_id, :auth_key, :word_check, @success);' ;            

            $command = Yii::$app->db->createCommand($sql);

            $command->bindParam(":game_id", $game_id, \PDO::PARAM_INT);
            $command->bindParam(":auth_key", $auth_key, \PDO::PARAM_STR);
            $command->bindParam(":word_check", $word, \PDO::PARAM_STR);
            $command->execute();
            $success = (bool)Yii::$app->db->createCommand("select @success;")->queryScalar();

         }catch (Exception $e) {

            Log::trace("Error : ".$e);

            throw new Exception("Error : ".$e);

         } 
		return $game;
	}
	
	public function actionInfo(){
		$request = Yii::$app->request;
		$game_id = (int)$request->getBodyParam('id');
		$auth_key = $request->getBodyParam('auth_key');		
		$game = null;		
		$game = Games::find()
		->select('start_word, status_id')		
		->where(['id' => $game_id, 'auth_pl1' => $auth_key])
		->asArray()
		->one();		
		if (!empty($game['start_word'])){
			$mySteps = ['words' => []];
			$tmpSteps = stepsGame::find()
			->select('{{words}}.word, {{steps_game}}.word_id')
			->joinWith('word')
			->where(['{{steps_game}}.game_id' => $game_id, '{{steps_game}}.player_id' => 0])			
			->all();			
			foreach($tmpSteps as $step){
				$mySteps['words'][] = $step->word->word;
			}
			$opponentSteps = ['words' => []];
			$tmpSteps = stepsGame::find()
			->select('{{words}}.word, {{steps_game}}.word_id')
			->innerJoinWith('word')
			->where(['game_id' => $game_id, 'player_id' => 1])						
			->all();			
			foreach($tmpSteps as $step){
				$opponentSteps['words'][] = $step->word->word;
			}			
			$game['mySteps'] = $mySteps;
			$game['opponentSteps'] = $opponentSteps;
		} else { 
			$game = Games::find()
			->select('start_word, status_id')
			->where(['id' => $game_id, 'auth_pl2' => $auth_key])
			->asArray()
			->one();
			if (!empty($game['start_word'])){
				$mySteps = ['words' => []];
				$tmpSteps = stepsGame::find()
				->select('{{words}}.word, {{steps_game}}.word_id')
				->innerJoinWith('word')
				->where(['game_id' => $game_id, 'player_id' => 1])						
				->all();			
				foreach($tmpSteps as $step){
					$mySteps['words'][] = $step->word->word;
				}
				$opponentSteps = ['words' => []];
				$tmpSteps = stepsGame::find()
				->select('{{words}}.word, {{steps_game}}.word_id')
				->innerJoinWith('word')
				->where(['game_id' => $game_id, 'player_id' => 0])						
				->all();			
				foreach($tmpSteps as $step){
					$opponentSteps['words'][] = $step->word->word;
				}
				$game['mySteps'] = $mySteps;
				$game['opponentSteps'] = $opponentSteps;
			}
		}
		return $game;
	}
}
