<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=bld-db;dbname=bld',
    'username' => 'bld_admin',
    'password' => 'bld',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
