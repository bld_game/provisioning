#!/bin/sh

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
chown www-data /var/www/html/api/web/assets
chown www-data /var/www/html/api/controllers
cd /var/www/html/api
composer install
php init --env=Development --overwrite=Yes
# php init --env=Production --overwrite=Yes

exec php-fpm
